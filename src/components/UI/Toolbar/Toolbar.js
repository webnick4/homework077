import React from 'react';
import { Navbar } from "react-bootstrap";

const Toolbar = () => (
  <Navbar>
    <Navbar.Header>
      <Navbar.Brand>
        Welcome to our open chat!
      </Navbar.Brand>
    </Navbar.Header>
  </Navbar>
);

export default Toolbar;