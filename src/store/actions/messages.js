import axios from '../../axios-api';
import { CREATE_MESSAGE_SUCCESS, FETCH_MESSAGES_SUCCESS } from "./actionTypes";

export const fetchMessagesSuccess = messages => {
  return { type: FETCH_MESSAGES_SUCCESS, messages };
};

export const fetchMessages = () => {
  return dispatch => {
    return axios.get('/messages').then(
      response => dispatch(fetchMessagesSuccess(response.data))
    );
  };
};

export const createMessageSuccess = () => {
  return { type: CREATE_MESSAGE_SUCCESS };
};

export const createMessage = (data) => {
  return dispatch => {
    return axios.post('/messages', data).then(
      response => dispatch(createMessageSuccess())
    );
  };
};