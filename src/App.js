import React, { Component, Fragment } from 'react';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Chat from "./containers/Chat/Chat";



class App extends Component {
  render() {
    return (
      <Fragment>
        <header>
          <Toolbar/>
        </header>
        <main className="container">
          <Chat />
        </main>
      </Fragment>
    );
  }
}

export default App;
