import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import {Button, Image, Modal, PageHeader, Panel} from "react-bootstrap";
import NewMessage from "../NewMessage/NewMessage";
import {fetchMessages} from "../../store/actions/messages";

class Chat extends Component {
  state = {
    showModal: false
  };

  handleClose = () => {
    this.setState({showModal: false});
  };

  openAddCommentModal = () => {
    this.setState({showModal: true});
  };

  componentDidMount() {
    this.props.onFetchMessages();
  }


  render() {
    return (
      <Fragment>
        <PageHeader>
          4chan
          <Button bsStyle="primary" className="pull-right" onClick={this.openAddCommentModal}>Add comment</Button>
        </PageHeader>

        {this.props.messages.map(message => (
          <Panel key={message.id}>
            <Panel.Body>
              { message.image && <Image
                  style={{width: '100px', marginRight: '10px', float: 'left'}}
                  src={'http://localhost:8000/uploads/' + message.image}
                  thumbnail
                />
              }

              <strong>Author: </strong> {message.author}

              <p style={{marginLeft: '10px'}}>
                {message.message}
              </p>
            </Panel.Body>
          </Panel>
        ))}

        <Modal show={this.state.showModal} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>New comment</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <NewMessage close={() => this.handleClose} />
          </Modal.Body>
        </Modal>
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    messages: state.products.messages
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchMessages: () => dispatch(fetchMessages())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);