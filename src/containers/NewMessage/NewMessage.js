import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import MessageForm from "../../components/MessageForm/MessageForm";
import {createMessage} from "../../store/actions/messages";


class NewMessage extends Component {
  createMessage = data => {
    this.props.onMessageCreated(data);
  };


  render() {
    return (
      <Fragment>
        <MessageForm onSubmin={this.createMessage} />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onMessageCreated: data => dispatch(createMessage(data))
  }
};

export default connect(null, mapDispatchToProps)(NewMessage);